Ông Nguyễn Minh Vũ là Chuyên gia bảo mật với hơn 10 năm kinh nghiệm trong lĩnh vực bảo mật, đam mê nghiên cứu và khắc phục lỗ hổng của các máy chấm công nhằm tăng khả năng bảo mật và an toàn cho sản phẩm. Từ năm 2018 đến nay, ông Nguyễn Minh Vũ đảm nhiệm vị trí CEO chịu trách nhiệm nội dung và xử lý kỹ thuật bảo mật tại ThietBiMayChamCong TBMCC.
Thông tin cá nhân:
Họ tên: Nguyễn Minh Vũ
Ngày sinh: 18/06/1982
Email: nguyenvumaychamcong@gmail.com
Chức vụ: CEO tại ThietBiMayChamCong TBMCC
Kinh nghiệm:
Bằng tốt nghiệp loại giỏi chuyên ngành Công nghệ thông tin tại Học Viện Kỹ Thuật Mật Mã Việt Nam năm 2006 
Kỹ sư bảo mật: 2006 - nay
Nhà nghiên cứu bảo mật: 2006 - nay
Từ 2018, trở thành CEO tại ThietBiMayChamCong TBMCC cung cấp và phân phối các dòng sản phẩm Máy chấm công giá rẻ, chính hãng như: máy chấm công vân tay, máy chấm công khuôn mặt, máy chấm công thẻ từ, máy chấm công thẻ giấy…
Ngoài ra, ông còn tham gia các công trình nghiên cứu nhằm nâng cao tính bảo mật cho các loại máy chấm công.
Với tầm nhìn và sứ mệnh đưa ThietBiMayChamCong TBMCC trở thành thương hiệu nổi tiếng hàng đầu trong và ngoài nước, cung cấp dòng máy chấm công với giá cả hợp lý. Nó giúp thay đổi suy nghĩ của người dùng trong việc ứng dụng công nghệ điện tử, đồng thời mang đến giải pháp an ninh tuyệt đối.
Liên kết với CEO Nguyễn Minh Vũ qua mạng xã hội:
https://twitter.com/nguyenvumcc 
https://www.pinterest.com/nguyenvumcc82/ 
https://www.reddit.com/user/nguyenvumcc 
https://flipboard.com/@nguyenvumcc 
https://www.scoop.it/u/nguyen-minh-vu-5 
https://www.plurk.com/nguyenvumcc 
https://infogram.com/ceo-nguyen-minh-vu-1h7j4dvo3qyn94n?live 
https://www.youtube.com/channel/UCM0smFq7flCp2XvHzaDLJJw/about 
https://www.facebook.com/nguyenvumcc/posts/108638531612435 
https://www.flickr.com/photos/194281702@N02/51639402527/in/dateposted-public/ 
https://www.behance.net/gallery/130333527/CEO-NGUYN-MINH-VU 
https://myspace.com/nguyenvumcc 
https://www.allmyfaves.com/nguyenvumcc 
https://linktr.ee/nguyenvumcc 
https://www.debate.org/nguyenvumcc/ 
https://www.doyoubuzz.com/minh-vu-nguy-n 
https://wakelet.com/wake/QyPZMUPtfQqc_TE_-hvKS 
https://www.symbaloo.com/shared/AAAACNOQOB8AA42AhBDSQg== 
https://about.me/nguyenvumcc/getstarted 
